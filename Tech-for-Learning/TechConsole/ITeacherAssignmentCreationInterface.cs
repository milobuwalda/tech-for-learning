﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechConsole
{
    interface ITeacherAssignmentCreationInterface
    {
        void createAssignment();
        void createVariables();
        void removeVariables();
        void addFeedbackToVariable();
        void RemoveFeedbackFromVariable();
        void addBuggyRule(); 
    }
}
