﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TechModels.Models;
using System.Xml;
using TechModels.Models.Parsers;
using TechModels.Database;
using System.Data.Entity;

namespace TechConsole
{
    class Program
    {
        private static IExpressionParser parser;
        private static TechEntities context;
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
            Database.SetInitializer<TechEntities>(null);
            parser = new CustomParser();
            context = new TechEntities();
            TechEntities.Initialize(context);
            Initialize();
        }


        private static void Initialize()
        {
            Assignment assignment = context.Assignments.First();
            
            assignment.WriteStory(); 
            Console.WriteLine("Variable indices:");
            for(int i = 0; i < assignment.Variables.Count; i++)
            {
                Console.WriteLine(string.Format("[{0}] {1}", i, assignment.Variables[i].Name));
            }
            Console.WriteLine("--------------------------------------------------------------------------------");

            while (true)
            {
                string s = Console.ReadLine();
                s = s.ToLower();
                if(s.StartsWith("feedback "))
                {
                    string[] split = Regex.Split(s, "feedback ");
                    int index = Int32.Parse(split[1]);
                    if(index >= 0 && index < assignment.Variables.Count)
                    {
                        if(assignment.Variables[index].Feedbacks.Count == 0)
                        {
                            Console.WriteLine("No feedback for this variable.");
                        }
                        /*
                        foreach (Feedback feedback in assignment.Variables[index].Feedbacks)
                        {
                            feedback.WriteFeedback();
                        } */
                        assignment.Variables[index].Feedbacks[0].WriteFeedback();
                    }
                }
                else if(s.StartsWith("ans "))
                {
                    string[] split = Regex.Split(s, "ans ");
                    string equation = split[1];
                    IExpression expression = parser.ParseString(assignment, equation);
                    if(expression != null)
                    {
                        if(expression.IsValid())
                        {
                            Console.WriteLine("Your representing answer: " + expression.ToString(false));
                            List<Feedback> attachedFeedback = new List<Feedback>();
                            foreach (Feedback feedback in assignment.Feedbacks)
                            {
                                if (feedback.Trigger.Equals(expression))
                                {
                                    attachedFeedback.Add(feedback);
                                }
                            }

                            
                            if (assignment.Answer.Equals(expression))
                            {
                                Console.WriteLine("Correct! You have the right answer.");
                            }
                            else
                            {
                                List<Feedback> universalFeedback = assignment.Answer.GetUniversalFeedback(expression);
                                attachedFeedback.AddRange(universalFeedback);
                                Console.WriteLine("Wrong answer.");
                                IValue expr = expression as IValue;
                                if (expr != null && assignment.Answer.ContainsExpression(expr))
                                {
                                    attachedFeedback.Add(new Feedback()
                                    {
                                        Text = "Your answer is a part of the final solution!"
                                    });
                                }
                            }

                            if (attachedFeedback.Count > 0)
                            {
                                Console.WriteLine();
                                Console.WriteLine("Feedback:");
                                int feedbackLevel = 2;
                                foreach (Feedback feedback in attachedFeedback.Where(f => f.Level == feedbackLevel || f.Level == 0))
                                {
                                    feedback.WriteFeedback();
                                }
                                Console.WriteLine();
                            }
                        }
                        else
                        {
                            Console.WriteLine("Given answer not valid.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Syntax error. Could not interpret given answer.");
                    }
                }
                else if(s.Contains("giveans"))
                {
                    if(s.Contains("val"))
                    {
                        Console.WriteLine(assignment.Answer.ToString(true));
                    }
                    else
                    {
                        Console.WriteLine(assignment.Answer.ToString(false));
                    }
                }
                Console.WriteLine("--------------------------------------------------------------------------------");
            }

            //Console.WriteLine(assignment.Answer + "= " + assignment.Answer.Value);
        }
    }
}
