﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace TechModels.Models.Readers
{
    public class ReadXML
    {
        public static List<Assignment> readData(XmlDocument doc)
        {
            List<Assignment> assignments = new List<Assignment>();

            foreach (XmlNode node in doc.GetElementsByTagName("assignment"))
            {
                Assignment assignment = new Assignment();

                //0 story, 1 ID, 2 variables, 3 answer, 4 feedbacklist

                assignment.Story = node.ChildNodes[0].InnerText;
                assignment.AssignmentId = int.Parse(node.ChildNodes[1].InnerText);

                List<AbstractVariable> varList = new List<AbstractVariable>();

                XmlNodeList variableList = node.ChildNodes[2].ChildNodes;

                foreach (XmlNode childNode in variableList)
                {
                    if (childNode.Name == "varInformative")
                    {
                        InformativeVariable variable = new InformativeVariable();
                        storeVariableFromNode(variable, childNode, varList);
                    }
                    else if (childNode.Name == "varValue")
                    {
                        ValueVariable variable = new ValueVariable();
                        storeVariableFromNode(variable, childNode, varList);
                    }
                }

                //varList.Sort((x, y) => x.VariableId.CompareTo(y.VariableId));

                foreach (XmlNode childNode in variableList)
                    getFeedbackFromNode(childNode, varList);

                assignment.Variables = new List<AbstractVariable>();
                for (int i = 0; i < varList.Count(); i++)
                    assignment.Variables.Add(varList.ElementAt(i));

                assignment.Type = AssignmentType.Value;

                XmlNode answerNode = node.ChildNodes[3];
                Expression answer = getExpressionFromNode(answerNode, varList);
                assignment.Answer = answer;

                XmlNode feedbackList = node.ChildNodes[4];
                addFeedback(feedbackList, assignment, varList);

                assignments.Add(assignment);
            }

            return assignments;
        }

        private static void addFeedback(XmlNode feedbackList, Assignment assignment, List<AbstractVariable> varList)
        {
            foreach(XmlNode node in feedbackList)
            {
                Feedback f = new Feedback();
                List<AbstractVariable> refVars = new List<AbstractVariable>();

                foreach(XmlNode childNode in node.ChildNodes)
                {
                    if (childNode.Name == "trigger") f.Trigger = getExpressionFromNode(childNode, varList);
                    else if (childNode.Name == "text") f.Text = childNode.InnerText;
                    else if (childNode.Name == "varRef") refVars.Add(varList.ElementAt(int.Parse(childNode.InnerText)));
                    else if (childNode.Name == "level") f.Level = int.Parse(childNode.InnerText);
                }
                f.VariableReferences = new List<AbstractVariable>();
                f.VariableReferences.AddRange(refVars);

                assignment.Feedbacks.Add(f);
            }
        }

        private static Expression getExpressionFromNode(XmlNode n, List<AbstractVariable> varList)
        {
            XmlNode leftNode = n.ChildNodes[0];
            XmlNode operatorNode = n.ChildNodes[1];
            XmlNode rightNode = n.ChildNodes[2];
            Expression e = new Expression();

            if (leftNode.ChildNodes.Count == 1) e.LeftExpression = (IValue)varList.ElementAt(int.Parse(leftNode.ChildNodes[0].InnerText));
            else e.LeftExpression = getExpressionFromNode(leftNode, varList);
            e.Operator = operatorFromNode(operatorNode.ChildNodes[0]);
            if (rightNode.ChildNodes.Count == 1) e.RightExpression = (IValue)varList.ElementAt(int.Parse(rightNode.ChildNodes[0].InnerText));
            else e.RightExpression = getExpressionFromNode(rightNode, varList);

            return e;
        }

        private static Operator operatorFromNode(XmlNode n)
        {
            switch(n.InnerText)
            {
                case "+":
                {
                    return Operator.Plus;
                }
                case "-":
                {
                    return Operator.Minus;
                }
                case "*":
                {
                    return Operator.Multiply;
                }
                case "/":
                {
                    return Operator.Divide;
                }
                case "(":
                    return Operator.OpenBracket;
                case ")":
                    return Operator.CloseBracket;
                case "=":
                    return Operator.Equals;
                default:
                {
                    throw new FormatException();
                }
            }
        }

        private static void getFeedbackFromNode(XmlNode n, List<AbstractVariable> varList)
        {
            foreach (XmlNode node in n.ChildNodes)
            {
                if (node.Name == "feedback")
                {
                    Feedback f = new Feedback();
                    int idx = 0;

                    foreach (XmlNode feedbackNode in node.ChildNodes)
                    {
                        if (feedbackNode.Name == "id")
                        {
                            idx = int.Parse(feedbackNode.InnerText);
                            varList[idx].Feedbacks = new List<Feedback>();
                            f.VariableReferences = new List<AbstractVariable>();
                        }
                        else if (feedbackNode.Name == "text") f.Text = feedbackNode.InnerText;
                        else if (feedbackNode.Name == "level") f.Level = int.Parse(feedbackNode.InnerText);
                        else if (feedbackNode.Name == "varRef") f.VariableReferences.Add(varList.ElementAt(int.Parse(feedbackNode.InnerText)));
                    }
                    
                    varList[idx].Feedbacks.Add(f);
                }
            }
        }

        private static void storeVariableFromNode(InformativeVariable v, XmlNode n, List<AbstractVariable> varList)
        {
            foreach (XmlNode node in n.ChildNodes)
            {
                if (node.Name == "name") v.Name = node.InnerText;
                else if (node.Name == "color") v.Color = node.InnerText;
                else if (node.Name == "id") v.VariableId = int.Parse(node.InnerText);               
            }
            varList.Add(v);
        }

        private static void storeVariableFromNode(ValueVariable v, XmlNode n, List<AbstractVariable> varList)
        {
            foreach (XmlNode node in n.ChildNodes)
            {
                if (node.Name == "name") v.Name = node.InnerText;
                else if (node.Name == "color") v.Color = node.InnerText;
                else if (node.Name == "value") v.Value = double.Parse(node.InnerText, System.Globalization.NumberFormatInfo.InvariantInfo);
                else if (node.Name == "id") v.VariableId = int.Parse(node.InnerText);
            }
            varList.Add(v);
        }
    }
}
