namespace TechModels.Database
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Models;
    using System.Xml;
    using System.Collections.Generic;
    using Models.Readers;
    using System.IO;

    public partial class TechEntities : DbContext
    {
        private static bool isInitialized = false;

        public DbSet<InformativeVariable> InformativeVariables { get; set; }
        public DbSet<AbstractVariable> AbstractVariables { get; set; }
        public DbSet<ValueVariable> ValueVariables { get; set; }
        public DbSet<Expression> Expressions { get; set; }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }



        public TechEntities()
            : base("name=TechEntities")
        {
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<TechEntities>(new DropCreateDatabaseAlways<TechEntities>());
            base.OnModelCreating(modelBuilder);
        }

        public static void Initialize(TechEntities context)
        {

            foreach(Assignment a in context.Assignments.ToList())
            {
                context.Assignments.Remove(a);
            }
            context.SaveChanges();
            
            isInitialized = true;

            if (true)
            {
                XmlDocument xml_doc = new XmlDocument();
                xml_doc.Load(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), @"Google Drive\Technologies for Learning 2016\Input\AssignmentsFromLiterature.xml"));
                List<Assignment> assignments = ReadXML.readData(xml_doc);
                foreach (Assignment a in assignments)
                {
                    context.Assignments.Add(a);
                }
                context.SaveChanges();
            }
            else
            {
                Assignment assignment = new Assignment();
                assignment.Story = "[[Pietje]] has [[200 apples]] of which he sells some of them, " +
                    "each at a [[decreasing price]] of [[0.20 cents]], starting from [[3 euro's]] each. \r\n " +
                    "He should stop selling when the price of his apples are [[0 euro's]]. \r\n " +
                    "How many apples does Pietje has left when the price of his apples have reach 0 euro's each?";
                InformativeVariable pietjeVariable = null;
                pietjeVariable = new InformativeVariable()
                {
                    Name = "Pietje",
                    Index = 0,
                    Color = "Red",
                    Feedbacks = new List<Feedback>()
                };
                pietjeVariable.Feedbacks.Add(new Feedback()
                {
                    Level = 1,
                    Text = "[[Pietje]] is the person selling the apples.",
                    VariableReferences = new List<AbstractVariable>()
                            {
                                pietjeVariable
                            }
                });
                context.InformativeVariables.Add(pietjeVariable);

                ValueVariable twoHundredApples = null;
                twoHundredApples = new ValueVariable()
                {
                    Name = "200 apples",
                    Color = "Green",
                    Index = 1,
                    Value = 200,
                    Feedbacks = new List<Feedback>()
                };
                twoHundredApples.Feedbacks.Add(new Feedback()
                {
                    Level = 1,
                    Text = "The [[200 apples]] [[Pietje]] has at the start of the assignment.",
                    VariableReferences = new List<AbstractVariable>()
                            {
                                twoHundredApples,
                                pietjeVariable
                            }
                });
                context.ValueVariables.Add(twoHundredApples);

                InformativeVariable decreasingVariable = new InformativeVariable()
                {
                    Name = "Decreasing",
                    Color = "Blue",
                    Index = 2,
                    Feedbacks = new List<Feedback>()
                    {
                        new Feedback()
                        {
                            Level = 1,
                            Text = "'Decreasing' means it gets less and less.",
                        }
                    }
                };
                context.InformativeVariables.Add(decreasingVariable);

                ValueVariable decreasingPriceVariable = null;
                decreasingPriceVariable = new ValueVariable()
                {
                    Name = "0.20 euro's",
                    Value = 0.20,
                    Index = 3,
                    Feedbacks = new List<Feedback>()
                };
                decreasingPriceVariable.Feedbacks.Add(new Feedback()
                {
                    Level = 1,
                    Text = "Every time [[Pietje]] sells an apple the price gets [[0.20 cents]] less.",
                    VariableReferences = new List<AbstractVariable>()
                            {
                                pietjeVariable,
                                decreasingPriceVariable
                            }
                });
                context.ValueVariables.Add(decreasingPriceVariable);

                ValueVariable threeEurosVariable = null;
                threeEurosVariable = new ValueVariable()
                {
                    Name = "3.00 euro's",
                    Value = 3.0,
                    Index = 4,
                    Feedbacks = new List<Feedback>()
                };
                threeEurosVariable.Feedbacks.Add(
                    new Feedback()
                    {
                        Level = 1,
                        Text = "The starting price of the apples is [[3.00 euro's]].",
                        VariableReferences = new List<AbstractVariable>()
                        {
                                threeEurosVariable
                        }
                    });
                context.ValueVariables.Add(threeEurosVariable);

                ValueVariable zeroEurosVariable = null;
                zeroEurosVariable = new ValueVariable()
                {
                    Name = "0.00 euro's",
                    Value = 0.0,
                    Index = 5,
                    Feedbacks = new List<Feedback>()
                };
                zeroEurosVariable.Feedbacks.Add(new Feedback()
                {
                    Level = 1,
                    Text = "The eventual price of the apples is [[0.00 euro's]].",
                    VariableReferences = new List<AbstractVariable>()
                            {
                                zeroEurosVariable
                            }
                });
                context.ValueVariables.Add(zeroEurosVariable);


                assignment.Variables = new List<AbstractVariable>()
                {
                    pietjeVariable,
                    twoHundredApples,
                    decreasingVariable,
                    decreasingPriceVariable,
                    threeEurosVariable,
                    zeroEurosVariable
                };

                assignment.Type = AssignmentType.Value;

                assignment.Answer = new Expression()
                {
                    LeftExpression = twoHundredApples,
                    Operator = Operator.Minus,
                    RightExpression = new Expression()
                    {
                        LeftExpression = threeEurosVariable,
                        Operator = Operator.Divide,
                        RightExpression = decreasingPriceVariable
                    }
                };

                assignment.Feedbacks.Add(new Feedback()
                {
                    Level = 2,
                    Text = "You should turn around [[3.00 euro's]] and [[0.20 euro's]].",
                    VariableReferences = new List<AbstractVariable>()
                            {
                                threeEurosVariable,
                                decreasingPriceVariable
                            },
                    Trigger = new Expression()
                    {
                        LeftExpression = twoHundredApples,
                        Operator = Operator.Minus,
                        RightExpression = new Expression()
                        {
                            LeftExpression = decreasingPriceVariable,
                            Operator = Operator.Divide,
                            RightExpression = threeEurosVariable
                        }
                    }
                });
                context.Assignments.Add(assignment);
                context.SaveChanges();
            }
        }
    }
}
