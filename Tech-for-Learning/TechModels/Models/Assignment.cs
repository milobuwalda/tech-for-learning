﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace TechModels.Models
{
    /// <summary>
    /// Assignment class containing the information about an assignment.
    /// </summary>
    public class Assignment
    {
        Dictionary<string, AbstractVariable> dictionary = null;

        public int AssignmentId { get; set; }
        public string Story { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public AssignmentType Type { get; set; }

        [JsonIgnore]
        public virtual List<AbstractVariable> Variables { get; set; }

        [JsonProperty(PropertyName = "Variables")]
        public Dictionary<int, AbstractVariable> VariablesDictionary
        {
            get
            {
                Dictionary<int, AbstractVariable> dic = new Dictionary<int, AbstractVariable>();
                foreach(AbstractVariable v in Variables)
                {
                    dic[v.AbstractVariableId] = v;
                }
                return dic;
            }
            set
            {
                Variables = value.Values.ToList();
            }
        }

        public virtual Expression Answer { get; set; }

        [JsonIgnore]
        public virtual List<Feedback> Feedbacks { get; set; }

        [JsonProperty(PropertyName = "Feedbacks")]
        public Dictionary<int, Feedback> FeedbacksDictionary
        {
            get
            {
                Dictionary<int, Feedback> dic = new Dictionary<int, Feedback>();
                foreach (Feedback f in Feedbacks)
                {
                    dic[f.FeedbackId] = f;
                }
                return dic;
            }
            set
            {
                Feedbacks = value.Values.ToList();
            }
        }

        public Assignment()
        {
            Feedbacks = new List<Feedback>();
        }

        [JsonIgnore]
        public Dictionary<string, AbstractVariable> VariableDictionary
        {
            get
            {
                if(dictionary != null)
                {
                    return dictionary;
                }
                dictionary = new Dictionary<string, AbstractVariable>();
                string[] split = Story.Split(new string[] { "[[" }, StringSplitOptions.RemoveEmptyEntries);
                if(split.Length != Variables.Count)
                {
                    throw new Exception("Amount of Variables not equal to amount of [[ ]].");
                }
                for (int i = 0; i < split.Length; i++)
                {
                    string[] splitLeft = Story.Split(new string[] { "[[" }, StringSplitOptions.RemoveEmptyEntries);
                    if(splitLeft.Length == 1)
                    {
                        dictionary.Add(splitLeft[0], Variables[i]);
                    }
                    else
                    {
                        throw new Exception("Inconsistent use of [[ ]].");
                    }
                }
                return dictionary;
            }
        }

        public void WriteStory()
        {
            ConsoleHelper.WriteText(Story, Variables);
        }
    }
}