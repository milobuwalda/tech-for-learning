﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechModels.Models.Parsers
{
    public interface IExpressionParser
    {
        /// <summary>
        /// Tries to parse a string into an IExpression given the assignment and the expression string.
        /// </summary>
        /// <param name="assignment">The assignment to check for.</param>
        /// <param name="expressionString">The expression string to check for.</param>
        /// <returns>The corresponding IExpression. Throws Exception if the given string is invalid/cannot be parsed.</returns>
        IExpression ParseString(Assignment assignment, string expressionString);
    }
}
