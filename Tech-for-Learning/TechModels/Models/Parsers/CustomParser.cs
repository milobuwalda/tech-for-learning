﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechModels.Models.Parsers
{
    public class CustomParser : IExpressionParser
    {

        /// <summary>
        /// Tries to parse a string into an IExpression given the assignment and the expression string.
        /// </summary>
        /// <param name="assignment">The assignment to check for.</param>
        /// <param name="expressionString">The expression string to check for.</param>
        /// <returns>The corresponding IExpression. Throws Exception if the given string is invalid/cannot be parsed.</returns>
        public IExpression ParseString(Assignment assignment, string expressionString)
        {
            IExpression leftSide = null;
            IExpression rightSide = null;
            Operator op = Operator.None;
            foreach (char c in expressionString.ToList())
            {
                if (expressionString.Length == 0)
                    break;

                if (c.Equals(' '))
                {
                    continue;
                }
                else
                {
                    if (Char.IsNumber(c))
                    {
                        int number = Int32.Parse(c.ToString());
                        if (leftSide != null)
                        {
                            if (expressionString.Length == 0)
                            {
                                rightSide = assignment.Variables[number];
                            }
                            else
                            {
                                rightSide = ParseString(assignment, expressionString);
                                break;
                            }
                        }
                        else
                        {
                            leftSide = assignment.Variables[number];
                            if (!(leftSide is AbstractVariable))
                            {
                                return leftSide;
                            }
                        }
                    }
                    else
                    {
                        foreach (Operator o in Enum.GetValues(typeof(Operator)))
                        {
                            if (c.ToString().Equals(o.GetSymbol()))
                            {
                                op = o;
                                break;
                            }
                        }
                    }
                    expressionString = expressionString.Substring(1);
                }
            }
            if (rightSide == null)
            {
                return leftSide;
            }
            else
            {
                return new Expression()
                {
                    LeftExpression = leftSide as IValue,
                    Operator = op,
                    RightExpression = rightSide as IValue
                };
            }
        }
    }
}
