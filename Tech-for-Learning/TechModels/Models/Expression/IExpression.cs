﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechModels.Models
{
    public interface IExpression
    {
        /// <summary>
        /// Used to serialize/deserialize given model from API.
        /// </summary>
        [JsonIgnore]
        object ToDto { get; }

        /// <summary>
        /// Check if given object is equal to object.
        /// </summary>
        /// <param name="obj">object to compare with.</param>
        /// <returns>True if the objects are equal. False otherwise.</returns>
        bool Equals(object obj);

        /// <summary>
        /// Get a list of universal feedback for a given expression.
        /// </summary>
        /// <param name="expression">The expression to check for feedback.</param>
        /// <returns>List of universal feedback.</returns>
        List<Feedback> GetUniversalFeedback(IExpression expression);

        /// <summary>
        /// Returns the IExpression in text.
        /// </summary>
        /// <param name="useValues">If true, returns the values of corresponding IExpression. If false, it will return the names of the variables.</param>
        /// <returns>The IExpression in text.</returns>
        string ToString(bool useValues = true);

        /// <summary>
        /// Check whether an expression is valid.
        /// </summary>
        /// <returns>True if valid, false otherwise.</returns>
        bool IsValid();
    }
}