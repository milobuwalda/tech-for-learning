﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechModels.Models
{
    public enum Operator
    {
        None = 0,
        Plus = 1,
        Minus = 2,
        Multiply = 3,
        Divide = 4,
        OpenBracket = 5,        //This should not be handled as operator, but might be integrated in the Expression class.
        CloseBracket = 6,       //This should not be handled as operator, but might be integrated in the Expression class.
        Equals = 7        
    }

    public static class OperatorExtensions
    {
        /// <summary>
        /// Extension function to retrieve Operator symbol.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string GetSymbol(this Operator o)
        {
            switch(o)
            {
                case Operator.Plus:
                    return "+";
                case Operator.Minus:
                    return "-";
                case Operator.Multiply:
                    return "x";
                case Operator.Divide:
                    return "/";
                case Operator.OpenBracket:
                    return "(";
                case Operator.CloseBracket:
                    return ")";
                case Operator.Equals:
                    return "=";
                case Operator.None:
                    return "~";
                default:
                    throw new IndexOutOfRangeException();
            }
        }
    }
}