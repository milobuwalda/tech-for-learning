﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechModels.Models
{
    public abstract class AbstractVariable : IExpression
    {
        public int VariableId { get; set; }

        public object ToDto
        {
            get
            {
                return this.VariableId;
            }
        }

        [JsonIgnore]
        public int AbstractVariableId
        {
            get
            {
                return this.VariableId;
            }
            set
            {
                this.VariableId = value;
            }
        }

        public string Color { get; set; }

        public int Index { get; set; }

        public string Name { get; set; }
        public virtual List<Feedback> Feedbacks { get; set; }

        public List<int> FeedbackReferenceIds
        {
            get
            {
                return Feedbacks.Select(f => f.FeedbackId).ToList();
            }
        }


        //public bool ContainsExpression(IExpression expression)
        //{
        //    InformativeVariable v = expression as InformativeVariable;
        //    if (v != null)
        //    {
        //        return this.Equals(v);
        //    }
        //    return false;
        //}


        public string ToString(bool useValues = true)
        {
            if(useValues)
            {
                ValueVariable value = this as ValueVariable;
                if(value != null)
                    return value.Value.ToString();
            }
            return this.Name;
        }

        public abstract List<Feedback> GetUniversalFeedback(IExpression expression);


        public abstract bool IsValid();
    }
}
