﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechModels.Models
{
    public class InformativeVariable : AbstractVariable
    {
        [JsonIgnore]
        public int InformativeVariableId
        {
            get
            {
                return this.VariableId;
            }
            set
            {
                this.VariableId = value;
            }
        }

        public override bool Equals(object obj)
        {
            InformativeVariable other = obj as InformativeVariable;
            if(other != null)
            {
                return this.Name != null && this.Name.Equals(other.Name);
            }
            return false;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public override List<Feedback> GetUniversalFeedback(IExpression expression)
        {
            List<Feedback> feedbackList = new List<Feedback>();
            if(!(expression is InformativeVariable))
            {
                feedbackList.Add(new Feedback()
                {
                    Text = "The assignments expects an informative answer, not a value or expression.",
                    Level = 1
                });
            }
            return feedbackList;
        }

        public override bool IsValid()
        {
            return true;
        }
    }
}
