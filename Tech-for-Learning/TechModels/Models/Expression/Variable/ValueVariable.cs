﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechModels.Models
{
    public class ValueVariable : AbstractVariable, IValue
    {
        [JsonIgnore]
        public int ValueVariableId
        {
            get
            {
                return this.VariableId;
            }
            set
            {
                this.VariableId = value;
            }
        }
        public double Value { get; set; }

        public bool ContainsExpression(IValue expression)
        {
            return expression.Value.Equals(this.Value);
        }

        public override bool Equals(object obj)
        {
            ValueVariable other = obj as ValueVariable;
            if(other != null)
            {
                return Value.Equals(other.Value);
            }
            return false;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public override List<Feedback> GetUniversalFeedback(IExpression expression)
        {
            List<Feedback> feedbackList = new List<Feedback>();

            if(expression is ValueVariable)
            {
                ValueVariable givenAns = expression as ValueVariable;
                if(!this.Equals(givenAns))
                {
                    feedbackList.Add(new Feedback()
                    {
                        Text = string.Format("[[{0}]] should be [[{1}]]", givenAns.Name, this.Name),
                        VariableReferences = new List<AbstractVariable>
                        {
                            givenAns, this
                        },
                        Level = 2
                    });
                }
            }

            return feedbackList;
        }


        public override bool IsValid()
        {
            return true;
        }
    }
}
