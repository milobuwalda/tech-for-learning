﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechModels.Models
{
    public interface IValue : IExpression
    {
        /// <summary>
        /// Check whether the value itself is equal to the given expression or whether one of the children is equal to to the given expression.
        /// </summary>
        /// <param name="expression">The expression to check for.</param>
        /// <returns>True if the current expression or one of its children is equal to the given expression. False otherwise.</returns>
        bool ContainsExpression(IValue expression);

        /// <summary>
        /// Value of the IValue class.
        /// </summary>
        double Value { get; set; }
    }
}
