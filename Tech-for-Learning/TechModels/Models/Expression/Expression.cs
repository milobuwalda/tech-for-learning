﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechModels.Models
{
    public class Expression : IValue
    {
        public int ExpressionId { get; set; }
        [JsonIgnore]
        public virtual IValue LeftExpression { get; set; }
        [JsonIgnore]
        public virtual IValue RightExpression { get; set; }

        [JsonProperty(PropertyName = "LeftExpression")]
        public object LeftExpressionObject
        {
            get
            {
                return LeftExpression.ToDto;
            }
            set
            {
                this.LeftExpression = (IValue)value;
            }
        }

        [JsonProperty(PropertyName = "RightExpression")]
        public object RightExpressionObject
        {
            get
            {
                return RightExpression.ToDto;
            }
            set
            {
                this.LeftExpression = (IValue)value;
            }
        }


        [JsonConverter(typeof(StringEnumConverter))]
        public Operator Operator { get; set; }



        public bool ContainsExpression(IValue expression)
        {
            return this.Equals(expression) || LeftExpression.ContainsExpression(expression) || RightExpression.ContainsExpression(expression);
        }

        public override bool Equals(object obj)
        {
            Expression other = obj as Expression;
            if(other != null)
            {
                return (LeftExpression.Equals(other.LeftExpression) && Operator.Equals(other.Operator) && RightExpression.Equals(other.RightExpression)) || this.Value == other.Value;
            }
            return false;
        }

        [JsonIgnore]
        public double Value
        {
            get
            {
                switch (Operator)
                {
                    case Models.Operator.Divide:
                        return LeftExpression.Value / RightExpression.Value;
                    case Models.Operator.Minus:
                        return LeftExpression.Value - RightExpression.Value;
                    case Models.Operator.Multiply:
                        return LeftExpression.Value * RightExpression.Value;
                    case Models.Operator.Plus:
                        return LeftExpression.Value + RightExpression.Value;
                    default:
                        throw new Exception("Unsupported Operator: " + Operator);
                }
            }
            set
            {
                throw new Exception("Value cannot be set customly. It's determined by its properties.");
            }
        }

        public object ToDto
        {
            get
            {
                return this;
            }
        }

        public string ToString(bool useValues = true)
        {
            return string.Format("{0} {1} {2}", LeftExpression is Expression ? " ( " + LeftExpression.ToString(useValues) + " ) " : LeftExpression.ToString(useValues), Operator.GetSymbol(), RightExpression is Expression ? " ( " + RightExpression.ToString(useValues) + " ) " : RightExpression.ToString(useValues));
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", LeftExpression is Expression ? " ( " + LeftExpression.ToString() + " ) " : LeftExpression.ToString(), Operator.GetSymbol(), RightExpression is Expression ? " ( " + RightExpression.ToString() + " ) " : RightExpression.ToString());
        }

        public List<Feedback> GetUniversalFeedback(IExpression expression)
        {
            List<Feedback> feedbackList = new List<Feedback>();

            if (expression is InformativeVariable)
            {
                feedbackList.Add(new Feedback()
                {
                    Level = 0,
                    Text = "The assignment expects an answer which represents a value."
                });
            }
            else if (expression is Expression)
            {
                Expression givenAnswer = expression as Expression;
                if(givenAnswer.LeftExpression.Equals(this.LeftExpression) && givenAnswer.RightExpression.Equals(this.RightExpression) && givenAnswer.Operator != this.Operator)
                {
                    feedbackList.Add(new Feedback()
                    {
                        Level = 1,
                        Text = "The left and right expressions are correct, but you should use a different operator. In the expression: " + expression.ToString(false)
                    });
                    feedbackList.Add(new Feedback()
                    {
                        Level = 2,
                        Text = string.Format("You should use the {0} operator instead of the {1} operator in the expression: {2}", this.Operator, givenAnswer.Operator, expression.ToString(false))
                    });
                }
                else if(!givenAnswer.LeftExpression.Equals(this.LeftExpression) && !givenAnswer.RightExpression.Equals(this.RightExpression) && givenAnswer.Operator == this.Operator)
                {
                    AbstractVariable leftVariable = givenAnswer.LeftExpression as AbstractVariable;
                    AbstractVariable rightVariable = givenAnswer.RightExpression as AbstractVariable;
                    List<AbstractVariable> referencedVars = new List<AbstractVariable>();
                    if(leftVariable != null)
                    {
                        referencedVars.Add(leftVariable);
                    }
                    if(rightVariable != null)
                    {
                        referencedVars.Add(rightVariable);
                    }

                    feedbackList.Add(new Feedback()
                    {
                        Level = 2,
                        Text = string.Format("You should switch + " + (leftVariable != null ? "[[{0}]]" : "{0}") + " and " + (rightVariable != null ? "[[{1}]]" : "{1}") + " in the expression: {2}.", givenAnswer.LeftExpression.ToString(false), givenAnswer.RightExpression.ToString(false), expression.ToString(false)),
                        VariableReferences = referencedVars
                    });
                }
                else
                {
                    feedbackList = feedbackList.Concat(this.LeftExpression.GetUniversalFeedback(givenAnswer.LeftExpression)).ToList();
                    feedbackList = feedbackList.Concat(this.RightExpression.GetUniversalFeedback(givenAnswer.RightExpression)).ToList();
                }
            }

            return feedbackList;
        }


        public bool IsValid()
        {
            return LeftExpression != null && RightExpression != null && LeftExpression.IsValid() && RightExpression.IsValid() && Operator != null;
        }
    }
}