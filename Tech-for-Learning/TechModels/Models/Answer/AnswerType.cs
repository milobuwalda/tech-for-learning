﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechModels.Models.Answer
{
    public enum AnswerType
    {
        Correct,
        Incorrect,
        PartiallyCorrect,
        SyntaxError
    }
}
