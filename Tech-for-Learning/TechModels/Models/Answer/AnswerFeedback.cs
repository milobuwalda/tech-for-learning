﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechModels.Models.Answer
{
    public class AnswerFeedback
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public AnswerType Type { get; set; }

        public List<Feedback> Feedback { get; set; }
    }
}
