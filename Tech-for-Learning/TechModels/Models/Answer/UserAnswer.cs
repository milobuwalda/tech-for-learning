﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechModels.Models.Answer
{
    public class UserAnswer
    {
        public string Expression { get; set; }
        public int AssignmentId { get; set; }
    }
}
