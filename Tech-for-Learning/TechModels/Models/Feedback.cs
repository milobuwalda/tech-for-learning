﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace TechModels.Models
{
    /// <summary>
    /// Feedback class respsonsible for containing feedback information.
    /// </summary>
    public class Feedback
    {
        private Dictionary<string, AbstractVariable> dictionary = null;

        public int FeedbackId { get; set; }
        public string Text { get; set; }

        [JsonIgnore]
        public virtual List<AbstractVariable> VariableReferences { get; set; }

        [JsonProperty(PropertyName = "VariableReferences")]
        public List<int> VariableReferenceIds
        {
            get
            {
                return this.VariableReferences != null ? this.VariableReferences.Select(v => v.AbstractVariableId).ToList() : new List<int>();
            }
        }

        public virtual Expression Trigger { get; set; }
        public int Level { get; set; }

        /// <summary>
        /// Parses the variables within the text. Returns the dictionary if it's already been set and parses otherwise.
        /// </summary>
        [JsonIgnore]
        public Dictionary<string, AbstractVariable> VariableDictionary
        {
            get
            {
                if (dictionary != null)
                {
                    return dictionary;
                }
                dictionary = new Dictionary<string, AbstractVariable>();
                string[] split = Text.Split(new string[] { "[[" }, StringSplitOptions.RemoveEmptyEntries);
                if (split.Length != VariableReferences.Count)
                {
                    throw new Exception("Amount of Variables not equal to amount of [[ ]].");
                }
                for (int i = 0; i < split.Length; i++)
                {
                    string[] splitLeft = Text.Split(new string[] { "[[" }, StringSplitOptions.RemoveEmptyEntries);
                    if (splitLeft.Length == 1)
                    {
                        dictionary.Add(splitLeft[0], VariableReferences[i]);
                    }
                    else
                    {
                        throw new Exception("Inconsistent use of [[ ]].");
                    }
                }
                return dictionary;
            }
        }

        /// <summary>
        /// WriteFeedback function to handle colour printing in a generic way.
        /// </summary>
        public void WriteFeedback()
        {
            ConsoleHelper.WriteText(Text, VariableReferences);
        }

    }
}