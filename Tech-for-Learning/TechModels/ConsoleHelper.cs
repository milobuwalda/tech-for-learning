﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechModels.Models;

namespace TechModels
{
    public class ConsoleHelper
    {
        public static void WriteText(string text, List<AbstractVariable> variables)
        {
            int currentCounter = -1;
            char lastChar = 'a';
            ConsoleColor orignalColor = Console.ForegroundColor;
            foreach (char c in text)
            {
                if (c == '[')
                {
                    if (lastChar == c)
                    {
                        currentCounter++;
                        if (!string.IsNullOrEmpty(variables.ElementAt(currentCounter).Color))
                        {
                            Console.ForegroundColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), variables.ElementAt(currentCounter).Color);
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                        }
                    }
                }
                else if (c == ']')
                {
                    if (lastChar == c)
                    {
                        Console.ForegroundColor = orignalColor;
                    }
                }
                else
                {
                    Console.Write(c);
                }

                lastChar = c;
            }
            Console.WriteLine();
        }
    }
}
