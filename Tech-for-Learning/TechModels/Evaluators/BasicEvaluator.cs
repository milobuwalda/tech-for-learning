﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechModels.Models;
using TechModels.Models.Answer;
using TechModels.Models.Parsers;

namespace TechModels.Evaluators
{
    /// <summary>
    /// Current evaluator used to evaluate expressions.
    /// </summary>
    public class BasicEvaluator : IEvaluator
    {
        private IExpressionParser parser;

        public BasicEvaluator()
        {
            this.parser = new CustomParser();
        }

        /// <summary>
        /// Evaluate a given expression for a given assignment.
        /// </summary>
        /// <param name="assignment">Assignment to evaluate for.</param>
        /// <param name="givenAnswer">Given answer of the user to evaluate.</param>
        /// <returns>Feedback on the given answer.</returns>
        public AnswerFeedback Evaluate(Assignment assignment, Models.Answer.UserAnswer givenAnswer)
        {
            AnswerFeedback answerToGive = new AnswerFeedback();
            string equation = givenAnswer.Expression;
            IExpression expression = parser.ParseString(assignment, equation);
            if (expression != null)
            {
                if (expression.IsValid())
                {
                    
                    List<Feedback> attachedFeedback = new List<Feedback>();
                    foreach (Feedback feedback in assignment.Feedbacks)
                    {
                        if (feedback.Trigger.Equals(expression))
                        {
                            attachedFeedback.Add(feedback);
                        }
                    }


                    if (assignment.Answer.Equals(expression))
                    {
                        answerToGive.Type = AnswerType.Correct;
                    }
                    else
                    {
                        answerToGive.Type = AnswerType.Incorrect;
                        List<Feedback> universalFeedback = assignment.Answer.GetUniversalFeedback(expression);
                        attachedFeedback.AddRange(universalFeedback);
                        IValue expr = expression as IValue;
                        if (expr != null && assignment.Answer.ContainsExpression(expr))
                        {
                            answerToGive.Type = AnswerType.PartiallyCorrect;
                        }
                    }
                    answerToGive.Feedback = attachedFeedback;
                }
                else
                {
                    answerToGive.Type = AnswerType.SyntaxError;
                }
            }
            else
            {
                answerToGive.Type = AnswerType.SyntaxError;
            }
            return answerToGive;
        }
    }
}
