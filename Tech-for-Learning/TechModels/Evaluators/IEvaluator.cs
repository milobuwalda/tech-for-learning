﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechModels.Models;
using TechModels.Models.Answer;

namespace TechModels.Evaluators
{
    public interface IEvaluator
    {
        /// <summary>
        /// Evaluate a given expression for a given assignment.
        /// </summary>
        /// <param name="assignment">Assignment to evaluate for.</param>
        /// <param name="givenAnswer">Given answer of the user to evaluate.</param>
        /// <returns>Feedback on the given answer.</returns>
        AnswerFeedback Evaluate(Assignment assignment, UserAnswer givenAnswer);
    }
}
